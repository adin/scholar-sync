# About 

I needed a way to sync information from Google Scholar. Thus, these scripts came to life. I use them as I need to keep an up-to-date mini CV with some citations information that changes quickly. Since I don't want to do it manually, I kind of [automated it](https://xkcd.com/1319/).

- `scrapper.py`: is a script to sync informatio from Google Scholar to local repository in `.bib` format. It adds the citation count into an `addendum` field within each matching item.

- `scrap-author.py`: is a script to retrieve information about the author citation's summary into a set of macros to use in LaTeX. (Right now this is limited to one macro only.)

# Instalation

## Requirements

These scripts depend on three libraries:

- `configargparse`
- `bibtexparser`
- `scholarly`

For an easy installation you can run

```bash
pip install -r requirements.txt
```

## Scripts

The scripts can be downloaded or cloned into a folder for their execution.

If you intend to use them directly in your machine they should be executable (`chmod +x`) and within your system's path.

# Use

## `scrapper.py`

The main scrappper will pull information from a given scholar (through his or her name), and will fetch all the publications and match them with a local list of publications. Then, it will proceed to add a field (called `addendum`) with the citation's information of that publication (it uses the default string `Citations: #`).

The parameters for this script are
- `--author`: the exact name of the author's Google Scholar.
- `--input`: the file (`.bib`) that should be used to sync the publications. Default value is `publicatons.bib`.
- `--output`: the output file name (`.bib`) that should be used. Default value is `new-publicatons.bib`.
- `--input-output`: use this version if you want to override the existing file.
- `--config`: all this parameters can be specified within a configuration file so you don't have to declare them all the time. The default file is called [`scrapper.conf`](scrapper.conf) within the local directory, but it can be changed with this parameter.

## `scrap-author.py`

A secondary scrapper that only retrieves information about the author's citations. It dumps the information within a `.tex` file as macros. 

Currently the supported information is:
- `\authorCitations`: the number of citations of the author.

The parameters for this script are
- `--author`: the exact name of the author's Google Scholar.
- `--extras`: the output file name (`.tex`) that should be used. Default value is `sumulaExtras.tex`. Note that the name changed only to support the same configuration file with the previous script.
- `--config`: all this parameters can be specified within a configuration file so you don't have to declare them all the time. The default file is called [`scrapper.conf`](scrapper.conf) within the local directory, but it can be changed with this parameter.