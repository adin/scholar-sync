#!/usr/bin/env python3

from scholarly import scholarly

import bibtexparser
# Unicode support
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode
from bibtexparser.bwriter import BibTexWriter

# import argparse
import configargparse

# Instantiate the parser
parser = configargparse.ArgumentParser(default_config_files=['./*.conf'], description='Synchronization tool for Google Scholar and local bib file.')

parser.add('-c', '--config', is_config_file=True, help='Path of the configuration file.', default='./scrapper.conf')
parser.add('-i', '--input', type=str, help='Input bibtex file: publications.bib.', default='publications.bib')
parser.add('-o', '--output', type=str, help="Output bibtex file: new-publications.bib.", default="new-publications.bib")
parser.add('-io', '--input-output', type=str, help='Input and output bibtex file: publications.bib. The same file will be overwritten. Note that this option overrides input and output options.', default='')
parser.add('-a', '--author', type=str, required=True, help='Author name (equal to the Google Scholar site) to find the entries.')

args, unknown = parser.parse_known_args()

if args.author:
  authorName = args.author

if args.input_output:
  bibFile = args.input_output
  outBibFile = args.input_output
else:
  bibFile = args.input
  outBibFile = args.output

print(f"Searching for {authorName}")
search_query = scholarly.search_author(authorName)
author = next(search_query).fill()

pubs = [ {'title': pub.bib['title'], 'citedby': pub.citedby if hasattr(pub, 'citedby') else 0 } for pub in author.publications ]

with open(bibFile) as bibfile:
  str = bibfile.read()
  parser = BibTexParser()
  parser.customization = convert_to_unicode
  # bibdb = bibtexparser.loads(str, parser = parser)
  bibdb = BibTexParser(common_strings=True, customization = None).parse(str)
  
  for entry in bibdb.entries:
    citations = [p['citedby'] for p in pubs if p['title'].lower().strip() == entry['title'].lower().strip()]
    if citations and citations[0] > 0:
      entry['addendum'] = 'Citations:~{}'.format(citations[0])

writer = BibTexWriter()
writer.indent = '  '
with open(outBibFile, 'w') as bibfile:
  bibfile.write(writer.write(bibdb))
