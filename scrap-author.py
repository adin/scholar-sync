#!/usr/bin/env python3

import scholarly

import bibtexparser
# Unicode support
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode
from bibtexparser.bwriter import BibTexWriter

# import argparse
import configargparse

# Instantiate the parser
parser = configargparse.ArgumentParser(default_config_files=['./*.conf'], description='Synchronization tool for Google Scholar and local tex file.')

parser.add('-c', '--config', is_config_file=True, help='Path of the configuration file.', default='./scrapper.conf')
parser.add('-e', '--extras', type=str, help="Output of the extra information file: sumulaExtras.tex.", default="sumulaExtras.tex")
parser.add('-a', '--author', type=str, required=True, help='Author name (equal to the Google Scholar site) to find the entries.')

args, unknown = parser.parse_known_args()

if args.author:
  authorName = args.author

outFile = args.extras

search_query = scholarly.search_author(authorName)
author = next(search_query).fill()

with open(outFile, 'w') as file:
  str = '\\def\\authorCitations{{{}}}\n'.format(author.citedby)
  file.write(str)