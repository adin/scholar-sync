#!/usr/bin/env python3

import bibtexparser
# Unicode support
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode
from bibtexparser.bwriter import BibTexWriter

# import argparse
import configargparse

# Instantiate the parser
parser = configargparse.ArgumentParser(default_config_files=['./*.conf'], description='Synchronization tool for Google Scholar and local tex file.')

parser.add('-c', '--config', is_config_file=True, help='Path of the configuration file.', default='./scrapper.conf')
parser.add('-i', '--input', type=str, help='Input bibtex file for publications: publications.bib.', default='publications.bib')
parser.add('-t', '--theses', type=str, help='Input bibtex file for theses: theses.bib.', default='theses.bib')
parser.add('-e', '--extras', type=str, help="Output of the extra information file: sumulaExtras.tex.", default="sumulaExtras.tex")
parser.add('-a', '--author', type=str, required=True, help='Author name (equal to the Google Scholar site) to find the entries.')

args, unknown = parser.parse_known_args()

def countEntry(db, type):
  return len([entry for entry in db.entries if entry['ENTRYTYPE'] == type])

def countTheses(db, type):
  return len([entry for entry in db.entries if entry['ENTRYTYPE'] == 'thesis' and entry['level'] == type])

bibFile = args.input
thesesFile = args.theses
outFile = args.extras

with open(bibFile) as bibfile:
  str = bibfile.read()
  parser = BibTexParser()
  parser.customization = convert_to_unicode
  # bibdb = bibtexparser.loads(str, parser = parser)
  bibdb = BibTexParser(common_strings=True, customization = convert_to_unicode).parse(str)
  
  with open(outFile, 'a') as outfile:
    outfile.write('\\def\\numArticles{{{}}}\n'.format(countEntry(bibdb, 'article')))
    outfile.write('\\def\\numConferences{{{}}}\n'.format(countEntry(bibdb, 'inproceedings')))
    outfile.write('\\def\\numBooks{{{}}}\n'.format(countEntry(bibdb, 'books')))
    outfile.write('\\def\\numChapters{{{}}}\n'.format(countEntry(bibdb, 'incollections')))

with open(thesesFile) as bibfile:
  str = bibfile.read()
  parser = BibTexParser()
  parser.customization = convert_to_unicode
  parser.ignore_nonstandard_types = False
  # bibdb = bibtexparser.loads(str, parser = parser)
  bibdb = BibTexParser(common_strings=True, customization = convert_to_unicode, ignore_nonstandard_types = False).parse(str)
  
  with open(outFile, 'a') as outfile:
    outfile.write('\\def\\numUndergradTheses{{{}}}\n'.format(countTheses(bibdb, 'undergrad')))
    outfile.write('\\def\\numMasterTheses{{{}}}\n'.format(countTheses(bibdb, 'master')))
    outfile.write('\\def\\numPhDTheses{{{}}}\n'.format(countTheses(bibdb, 'doctor')))
